﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;


namespace DocsMetaBinder
{
    class Facade
    {
        static async Task Main(string[] args)
        {
            string AuthCredentials = Environment.GetEnvironmentVariable("user:pass");
            string AuthType = Environment.GetEnvironmentVariable("auth_type");
            string URL = Environment.GetEnvironmentVariable("api_endpoint") +
                        Environment.GetEnvironmentVariable("project") + "/" +
                        Environment.GetEnvironmentVariable("item") + "?" +
                        Environment.GetEnvironmentVariable("item_uri");

            Poolparty.ThesaurusApiConnect conn = new Poolparty.ThesaurusApiConnect();
            Poolparty.Models.ResponseWithStatusCode e = await conn.GetTopConcept(URL, AuthCredentials, AuthType);
            Console.WriteLine("Count:" + e.content.Count);
            Console.WriteLine("Top uri:" + e.content.First().uri);
            Console.WriteLine("Top label:" + e.content.First().prefLabel);

        }
    }
}
