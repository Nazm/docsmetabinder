﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocsMetaBinder.Poolparty.Models
{
    class ResponseWithStatusCode
    {
        public List<TopConceptMapping> content { get; set; } = null;//default set to null
        public int statusCode { get; set; } = -1;
        public string errorMessage { get; set; } = null;
    }
}
