﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace DocsMetaBinder.Poolparty
{
    class TopConceptMapping
    {
        [JsonProperty("uri")]
        public string uri { get; set; }

        [JsonProperty("prefLabel")]
        public string prefLabel { get; set; }

        [JsonProperty("altLabels")]
        public string altLabels { get; set; }

        [JsonProperty("hiddenLabels")]
        public string hiddenLabels { get; set; }

        [JsonProperty("definitions")]
        public string definitions { get; set; }

        [JsonProperty("broaders")]
        public string broaders { get; set; }

        [JsonProperty("narrowers")]
        public string narrowers { get; set; }

        [JsonProperty("relateds")]
        public string relateds { get; set; }

        [JsonProperty("conceptSchemes")]
        public string conceptSchemes { get; set; }
    }
}
