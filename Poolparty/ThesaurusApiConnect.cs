﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace DocsMetaBinder.Poolparty
{
    class ThesaurusApiConnect
    {
        public async Task<Models.ResponseWithStatusCode> GetTopConcept(string url, string authCredentials, String authType)
        {

            HttpClientHandler handler = new HttpClientHandler();
            HttpClient client = new HttpClient(handler);

            var byteArray = Encoding.ASCII.GetBytes(authCredentials);
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(authType, Convert.ToBase64String(byteArray));

            HttpResponseMessage response = await client.GetAsync(url);

            Models.ResponseWithStatusCode responseMapped = new Models.ResponseWithStatusCode();
            responseMapped.statusCode = (int)response.StatusCode;

            if (responseMapped.statusCode == 200)
            {
                HttpContent content = response.Content;
                string result = await content.ReadAsStringAsync();
              
                if (result != null)
                {
                    List<TopConceptMapping> topConcepts = new List<TopConceptMapping>();
                    try
                    {
                       topConcepts = JsonConvert.DeserializeObject<List<TopConceptMapping>>(result);
                    }
                    catch
                    {
                        responseMapped.errorMessage = "Unable to map JSON into class object";
                        return responseMapped;
                    }
                    responseMapped.content = topConcepts;
                    return responseMapped;
                }
                else 
                { 
                    responseMapped.errorMessage = "NULL content in api response";
                    return responseMapped;
                }
            }
            else
            {
                responseMapped.errorMessage = "Error getting response from API";
                return responseMapped;
            }
        }
         
    }
}
